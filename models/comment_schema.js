"use strict"
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
let commentSchema = new mongoose.Schema({
    userid: { type: String, ref:'signup' },
    comment: { type: String },
    blogid : { type: String, ref:'blog'},
    date: { type: Date },
    rating: {type:Number},
    is_delete : {type: Boolean}

});
const comment= mongoose.model('comment', commentSchema);
module.exports = comment;
