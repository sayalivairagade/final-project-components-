'use strict';

const  mongoose = require('mongoose');

var Schema = mongoose.Schema;

const  PaymentSchema = new mongoose.Schema({
    paymentID:{type: String},
    transaction_id: {type: String,required: true},
    user_id: {type: String,ref: 'signup'},
    product_id:  { type: String , ref:'product' },
    total_amount: {type:Number},
    last_four_digit_card: {type:String},
    card_holder_name: {type:String},
    payment_status: {type: Number,enum: [1, 2, 3] }, // 1 completed 2 pending 3 rejected
    payment_date: {type: Date},
    is_delete : {type: Boolean}
});

const Payment = mongoose.model('Payment', PaymentSchema);
module.exports = Payment;