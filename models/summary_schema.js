'use strict';

const  mongoose = require('mongoose');

var Schema = mongoose.Schema;

const  SummarySchema = new mongoose.Schema({
    finalCart:{type:Array},
    finalShipping:{type:Array},
    userid:{type:String},
    transaction_id: {type: String,required: true},
    total_amount: {type:Number},
    is_delete : {type: Boolean, default:false},
    date: {type: Date,default:new Date}

});

const Summary = mongoose.model('Summary', SummarySchema);
module.exports = Summary;