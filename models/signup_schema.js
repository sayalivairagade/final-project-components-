"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var formdata = new mongoose.Schema({
    fullname: { type: String },
    email: { type: String, unique:true },
    password: { type: String },
    role: { type: String },
    // cart:[{
    //     items:{
    //         type:string,
    //         ref:"product"
    //     }
    // }]
});

var signup = mongoose.model('signup', formdata);
module.export = signup;