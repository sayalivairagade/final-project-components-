"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var productdata = new mongoose.Schema({
    productname: { type: String },
    productcompanyname: { type: String },
    productdescription: { type: String},
    productcategory:{ type :String },
    productcost: { type: Number },
    image:{type : String}
});

var product = mongoose.model('product', productdata);
module.export = product;