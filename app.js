'use strict';

var SwaggerExpress = require('swagger-express-mw');
require('./config/db');
var utils = require('./api/lib/util');
var mongoose = require('mongoose');
var app = require('express')();
var express =require("express");
const bcrypt = require('bcrypt');
module.exports = app; // for testing

var config = {
  appRoot: __dirname // required config
};

app.use(express.static('public'));

SwaggerExpress.create(config, function(err, swaggerExpress) {
  if (err) { throw err; }

  app.use(function (req, res, next) {
    // CORS headers
    res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    // Set custom headers for CORS
    res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,If-Modified-Since,Authorization');
    
    if (req.method == 'OPTIONS') {
    res.status(200).end();
    } else {
    next();
    }
    });
    
    app.use('/api/*', function(req, res, next) {
      var freeAuthPath = [
        '/api/signupdata',
        '/api/logindata',
      ];
      var available = false;
      for (var i = 0; i < freeAuthPath.length; i++) {
      if (freeAuthPath[i] == req.baseUrl) {
      available = true;
      break;
    }
    }
    if (!available) {
    utils.ensureAuthorized(req, res, next);
    } else {
    next();
    }
    });
    
app.use(swaggerExpress.runner.swaggerTools.swaggerUi());
  // install middleware

  swaggerExpress.register(app);

  var port = process.env.PORT || 10010;
  app.listen(port);

  if (swaggerExpress.runner.swagger.paths['/hello']) {
    console.log('Server started at port : ' + port);
  }
});
