"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var addtocart = new mongoose.Schema({
    product_id : { type: String , ref:'product' },
    date: { type: Date },
    user_id: {type : String , ref:'signup'},
    productPrice: {type : Number},
    quantity: {type : Number},
    is_delete : {type: Boolean}
});

var cart = mongoose.model('cart', addtocart);
module.export = cart;