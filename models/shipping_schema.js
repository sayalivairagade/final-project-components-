'use strict';

var mongoose = require('mongoose');
var ShippingSchema = new mongoose.Schema({

    user_id: {
        type: String,    
     },
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    email: {
        type: String,
    },
    contact: {
        type: String,
    },
    address_line_1: {
        type: String,
    },
    address_line_2: {
        type: String,
    },
    state: {
        type: String,
    },
    country: {
        type: String,
    },
    city: {
        type: String,
    },
    zipcode: {
        type: String,
    },
    shipping_date: {
        type: Date 
    },
    is_deleted: {
        type: Boolean,
    }
})
var Shipping = mongoose.model('Shipping', ShippingSchema);
module.exports = Shipping;