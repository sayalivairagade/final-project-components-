// In this controller Add to Cart api was written

var mongoose = require('mongoose');
var cartform = mongoose.model('cart');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {
    addCart: addCart,
    listCart: listCart,
    deleteFromCart: deleteFromCart,
    updateCart: updateCart,
    removeallcartitem: removeallcartitem

}
// This api for Add the products into Add to cart
function addCart(req, res) {
    var record = new cartform();
    record.product_id = req.body.product_id;
    record.date = Date.now();
    record.user_id = req.body.user_id;
    record.productPrice = req.body.productPrice;
    record.quantity = 1;
    record.is_delete = false;

    cartform.findOne({
        product_id: req.body.product_id,
        is_delete: false
    }, function (err, result) {
        if (err) {
            consol.log(err);
        } else if (result) {
            console.log(result);
            quant = result.quantity + 1

            totalPrice = quant * record.productPrice;
            cartform.findOneAndUpdate({
                product_id: req.body.product_id,
                is_delete: false
            }, {
                $set: {
                    productPrice: totalPrice,
                    quantity: quant
                }
            }, function (err, result) {
                if (err) {
                    console.log(err);
                } else {
                    res.json({
                        code: 200,
                        message: "success",
                        data: result
                    })
                }
            })
        } else {
            record.save(function (err, response) {
                if (err) {
                    res.json({
                        code: 404,
                        message: 'comment not Added',
                        // data:response
                    })
                } else {
                    res.json({
                        code: 200,
                        data: response
                    })

                }
            })

        }
    })

}
productList = new Array;

// To list the Add to Cart products
function listCart(req, res) {
    var user_id = req.swagger.params.id.value
    cartform.find({
        user_id,
        is_delete: false
    }).sort({
        date: 'descending'
    }).populate({
        path: 'product_id',
        model: 'product',
    }).exec(function (err, data) {
        if (err) {
            console.log(err);
            res.json({
                code: 500,
                message: "unsuccess",
                data: err
            })
        } else if (data) {
            totalprice = 0;
            for (i = 0; i < data.length; i++) {
                totalprice = totalprice + data[i].productPrice
            }
        } 
        res.json({
            code: 200,
            message: "success",
            data: data,
            totalprice: totalprice
        })
    })
}

// List the Product from Cart
function deleteFromCart(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    cartform.findByIdAndRemove({
        _id: id
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}

// For Edit the Cart
function updateCart(req, res) {
    let user_id = req.body.user_id;
    let product_id = req.body.product_id;
    let quantity = req.body.quantity;
    let productPrice = quantity * req.body.productPrice;
    cartform.findOneAndUpdate({
        product_id,
        user_id,
        is_delete: false
    }, {
        $set: {
            productPrice: productPrice,
            quantity: quantity,
        }
    }, function (err, result) {
        if (err) {
            console.log(err);
        } else {
            res.json({
                code: 200,
                message: "success",
                data: result
            })
        }
    })
}

// This api For After Payment Successfully Remove all the items from Cart
function removeallcartitem(req, res) {
    userid = req.swagger.params.id.value;
    cartform.remove({
        user_id: userid
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    })
}