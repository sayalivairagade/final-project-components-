"use strict"
var mongoose = require('mongoose');
mongoose.connect('mongodb://127.0.0.1:27017/Myproject', { useNewUrlParser: true });
require('../models/signup_schema');
require('../models/product_schema');
require('../models/blog_schema');
require('../models/comment_schema');
require('../models/addcart_schema');
require('../models/payment_schema');
require('../models/shipping_schema');
require('../models/summary_schema');




//Get the default connection
var db = mongoose.connection;

//Bind connection to error event (to get notification of connection errors)
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function(){
console.log("Database Conected Succesfully");
})