"use strict"
var mongoose = require('mongoose');

//Define a schema
var Schema = mongoose.Schema;

var blogdata = new mongoose.Schema({
    title: { type: String },
    blog : { type: String },
    image: {type : String}

});

var blog = mongoose.model('blog', blogdata);
module.export = blog;