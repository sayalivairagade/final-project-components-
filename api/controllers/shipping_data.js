// In this controller Shipping address api was written.
var mongoose = require('mongoose');
var shippingform = mongoose.model('Shipping');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {

    shippingDetail: shippingDetail,

}

// This api for Shipping address.
function shippingDetail(req, res) {
    var shipform = new shippingform({
        user_id: req.body.user_id,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        email: req.body.email,
        contact: req.body.contact,
        address_line_1: req.body.address_line_1,
        address_line_2: req.body.address_line_2,
        state: req.body.state,
        country: req.body.country,
        city: req.body.city,
        zipcode: req.body.zipcode,
        shipping_date: Date.now(),
        is_delete: false
    });
    shipform.save(function (err, content) {
        if (err) {
            console.log(err);
            res.json({
                "code": 400,
                "message": "error"
            });
        } else {

            res.json({
                "code": 200,
                message: "success",
                "data": content
            });
        }
    });

}

