// In this controller Comments for Blog api was written 

var mongoose = require('mongoose');
var commentform = mongoose.model('comment');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {
    addComment: addComment,
    listComment: listComment,
    deleteComment: deleteComment,
}

// This api for Add comments for blog
function addComment(req, res) {
    var record = new commentform();
    record.comment = req.body.comment; //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
    record.date = Date.now();
    record.blogid = req.body.blogid;
    record.is_delete = false;
    record.userid = req.body.userid;
    record.rating = req.body.rating;
    record.save(function (err, response) {
        if (err) {
            res.json({
                code: 404,
                message: 'comment not Added'
            })
        } else {
            res.json({
                code: 200,
                data: response
            })
            console.log('Successfully created');

        }

    })
}

// This api for List all comments 
function listComment(req, res) {
    commentform.find({
        blogid: req.swagger.params.id.value,
        is_delete: false
    }).sort({
        date: 'descending'
    }).populate({
        path: 'blogid',
        model: 'blog',
    }).populate({
        path: 'userid',
        model: 'signup',

    }).exec(function (err, data) {
        console.log("After Listing data", data);
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }    })
}

// This api for delete the comments 
function deleteComment(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    commentform.findByIdAndRemove(id, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}