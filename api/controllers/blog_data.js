// Blog Api is written in this component for Add blog, list Blog , delete , edit and get previous detail blog getdetail api was written

var mongoose = require('mongoose');
var blogform = mongoose.model('blog');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {
    addBlog: addBlog,
    listBlog: listBlog,
    deleteBlog: deleteBlog,
    editBlog: editBlog,
    getBlogDetail: getBlogDetail,
}
// For Add a blog

function addBlog(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var image = '../server/public/blogupload/' + orignalImageName;
    fs.writeFile(image, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var product = new blogform();
    product.title = req.body.title;
    product.blog = req.body.blog;
    product.image = "http://localhost:10010/blogupload/" + orignalImageName;

    product.save(function (err, data) {
        if (err) {
            res.json({
                "code": 400,
                "message": "error"
            });
        } 
        else {

            res.json({
                "code": 200,
                "message": "success"
            });
        }
    });
}

// For list a Blog

function listBlog(req, res) {
    blogform.find(function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }    })
}

function deleteBlog(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    blogform.findByIdAndRemove({
        _id: id
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}

// For Edit a blog

function editBlog(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);


    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var image = '../server/public/blogupload/' + orignalImageName;
    fs.writeFile(image, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Blog Image Uploaded");
    })

    var title = req.body.title;
    var blog = req.body.blog;
    var image = "http://localhost:10010/blogupload/" + orignalImageName;



    blogform.findByIdAndUpdate(_id, {
        $set: {
            title: title,
            blog: blog,
            image: image

        }
    }, function (err, data) {
        if (err) {
            console.log("errr", err)
        } else {
            console.log("data", data)
        }
        data.save();
        res.json({
            data: data,
            code: 200
        });
    });
};

// For Get the detail of previous blog

function getBlogDetail(req, res) {
    var _id = req.swagger.params.id.value
    blogform.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                code:400,
                message: "not working"
            })
        } else if (userData) {
            res.json({
                code:200,
                message: "working",
                data: userData
            })
        } else {
            res.json({
                code:500,
                message: "not getting",
            })
        }
    })
}
