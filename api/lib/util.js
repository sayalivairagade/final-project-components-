'use strict';
var jwt = require('jsonwebtoken'),
mongoose = require('mongoose'),
form = mongoose.model('signup');

module.exports = {
    ensureAuthorized: ensureAuthorized
}

function ensureAuthorized(req, res, next) {
   // console.log(req.headers);
    var unauthorizedJson = {code: 401, 'message': 'Unauthorized', data: {}};
    var token = req.headers["authorization"] || req.query["api_key"];

    //if (req.headers.authorization) {
    if (typeof token !== 'undefined') {
        //var token = req.headers.authorization;
        var splitToken = token.split(' ');
        try {

            token = splitToken[1];
            var decoded = jwt.verify(token, 'shhhhh');
            if (splitToken[0] == 'Bearer') {
                req.user = decoded;
                form.findOne().exec({_id:decoded._id},function(err, user) {
                    if (err || !user) {
                        res.json(unauthorizedJson);
                    } else {
                        req.user = user;
                        next();
                    }
                });
            } else {
                res.json(unauthorizedJson);
            }
        } catch (err) {
            res.json(unauthorizedJson);
        }
    } else {
        res.json(unauthorizedJson);
    }
}