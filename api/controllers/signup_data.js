// In this controller user related api was written

var mongoose = require('mongoose');
var form = mongoose.model('signup');
// var crypto = ('../lib/crypto');
const bcrypt = require('bcrypt');
const stripe = require('stripe')('sk_test_k2MMSEjQnuOqrN7Hhehqy2eH');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {
    signupdata: signupdata,
    logindata: logindata,
    listuser: listuser,
    addUser: addUser,
    deleteUser: deleteUser,
    editUser: editUser,
    getUserDetail: getUserDetail,
    oldPassword:oldPassword,
    newPasswordChange:newPasswordChange,
    editUserProfile :editUserProfile
}

// This api for signup user
function signupdata(req, res) {
    const saltRounds = 10;
    const myPlaintextPassword = req.body.password;
    bcrypt.hash(myPlaintextPassword, saltRounds, function (err, hash) {
        if (err) {
            console.log("error", err);
        } else {
            var content = req.body;
            var Form = new form({
                fullname: req.body.fullname,
                email: req.body.email,
                password: hash,
                role: "user",
            });
            Form.save(function (err, content) {
                if (err) {
                    res.json({
                        "code": 400,
                        "message": "Email Already Exist"
                    });
                } else {

                    res.json({
                        "code": 200,
                        "message": "success"
                    });
                }
            });
        }
    });
}

// This api for Login of user.
function logindata(req, res) {
    const saltRounds = 10;
    var myPlaintextPassword = req.body.password;

    let data = {
        email: req.body.email,

        // password: req.body.password,
        // isdelete: false

    };
    form.findOne(data).lean().exec(function (err, user) {
        var hashPassword = user.password;
        if (err) {
            console.log("errrorrrrrrrrrrrrrrrr", err)
            return res.json({
                error: true
            });

        }

        if (!user) {
            console.log("invalid user", user)
            return res.status(404).json({
                'message': 'User not found!'
            });

        }

        bcrypt.compare(myPlaintextPassword, hashPassword, function (err, response) {
            console.log("gyiuwgsdfriuewgkfth", response);
            if (response == true) {
                //var id = user._id;

                let token = jwt.sign({
                    "id": user._id
                }, 'shhhhh', {

                    expiresIn: '3h' // expires in 3 hour

                });
                // console.log(token);
                res.json({
                    code : 200,
                    msg: 'Token is created',
                    token: token,
                    data: user
                });
            } else {
                res.json({
                    code : 500,
                    message: 'Incorrect username or password'
                });
            }
        });
    })

}

// This api for listing the user which was register
function listuser(req, res) {
    form.find(function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
        
    })
}

// This api for add the user. Which was performed by admin.
function addUser(req, res) {
    let adduserdata = new form({
        fullname: req.body.fullname,
        email: req.body.email,
        password: req.body.password,
        role: req.body.role,
    });
    adduserdata.save(function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}

// This api for delete a users.
function deleteUser(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    form.findByIdAndRemove({
        _id: id
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}

// This api for edit the user.
function editUser(req, res) {
    let _id = req.swagger.params.id.value
    var fullname = req.body.fullname;
    var email = req.body.email;
    var password = req.body.password;
    var role = req.body.role;

    form.findByIdAndUpdate(_id, {
        $set: {
            fullname: fullname,
            email: email,
            password: password,
            role: role
        }
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
};

// This api for get the user previous detail.
function getUserDetail(req, res) {
    var _id = req.swagger.params.id.value
    form.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                code:400,
                message: "not working",

            })
        } else if (userData) {
            res.json({
                code:200,
                message: "working",
                data: userData
            })
        } else {
            res.json({
                code:500,
                message: "not getting",
            })
        }
    })
}

// this api for reset password.For checking the User old password
function oldPassword(req, res) {
    var _id = req.swagger.params.id.value
    var password = req.body.password;
    form.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                message: "error",
                data: err
            })
        } else {
            var oldPassword = userData.password;
            bcrypt.compare(password, oldPassword, function (err, response) {
                if (response == true) {
                    res.json({ code: 200, message: 'Password Correct' });
                }
                else {
                    res.json({ code: 500, message: 'Password not Correct' });
                }
            });
        }
    })
}

// This api for reset the user password.Create a new password.
function newPasswordChange(req, res) {
    let _id = req.swagger.params.id.value
    const saltRounds = 10;
    const myPlaintextPassword = req.body.password;
    bcrypt.hash(myPlaintextPassword, saltRounds, function (err, hash) {
        if (err) {
            console.log("error", err);
        }
        else {
            form.findByIdAndUpdate(_id, { $set: { password: hash } }, function (err, data) {
                if (err) {
                    console.log("err", err)
                } else {
                    console.log("data", data)
                }
                data.save();
                res.json({ data: data, code: 200 });
            });
        }
    });
}

// this api for edit the user profile.
function editUserProfile(req, res) {
    let _id = req.swagger.params.id.value
    console.log(_id);
    var fullname = req.body.fullname;
    var email = req.body.email;

    form.findByIdAndUpdate(_id, {
        $set: {
            fullname: fullname,
            email: email,
        }
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
};


