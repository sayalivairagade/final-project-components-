// In this controller Product inventory api was written.

var mongoose = require('mongoose');
var productform = mongoose.model('product');
var fs = require("fs");
var jwt = require('jsonwebtoken');

module.exports = {
    addProductData: addProductData,
    listProduct: listProduct,
    listJeans: listJeans,
    deleteProduct: deleteProduct,
    editProduct: editProduct,
    getProductDetail: getProductDetail,
}
// This api for Add a products.
function addProductData(req, res) {
    //Uplaod Image
    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var image = '../server/public/productupload/' + orignalImageName;
    fs.writeFile(image, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })

    var product = new productform();
    product.productname = req.body.productname;
    product.productcompanyname = req.body.productcompanyname;
    product.productdescription = req.body.productdescription;
    product.productcategory = req.body.productcategory;
    product.productcost = req.body.productcost;
    product.image = "http://localhost:10010/productupload/" + orignalImageName;

    product.save(function (err, data) {
        if (err) {
            res.json({
                code: 400,
                message: "error"
            });
        } else {

            res.json({
                code: 200,
                message: "success"
            });
        }
    });
}

// This api for list the shirt category product
function listProduct(req, res) {
    productform.find({
        productcategory: "top"
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    })
}

// This api for list the jeans category products
function listJeans(req, res) {
    productform.find({
        productcategory: "jeans"
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    })
}

// This api for delete the products
function deleteProduct(req, res) {
    var id = req.swagger.params.id.value;
    console.log("..id", id)
    productform.findByIdAndRemove({
        _id: id
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
}

// This api for edit the products
function editProduct(req, res) {
    let _id = req.swagger.params.id.value

    timeStamp = Date.now();
    orignalImageName = timeStamp + "_" + req.files.file[0].originalname;
    var image = '../server/public/productupload/' + orignalImageName;
    fs.writeFile(image, (req.files.file[0].buffer), function (err) {
        if (err) throw err;
        console.log("Product Image Uploaded");
    })
    var productname = req.body.productname;
    var productcompanyname = req.body.productcompanyname;
    var productdescription = req.body.productdescription;
    var productcategory = req.body.productcategory;
    var productcost = req.body.productcost;
    var image = "http://localhost:10010/productupload/" + orignalImageName;

    productform.findByIdAndUpdate(_id, {
        $set: {
            productname: productname,
            productcompanyname: productcompanyname,
            productdescription: productdescription,
            productcategory: productcategory,
            productcost: productcost,
            image: image
        }
    }, function (err, data) {
        if(err){
            res.json({code: 400,message: 'error',data: err});
        }
        else{
            res.json({code: 200,message: 'success',data: data});
        }
    });
};

// This api for get the detail of products.
function getProductDetail(req, res) {
    var _id = req.swagger.params.id.value
    productform.findById(_id, function (err, userData) {
        if (err) {
            res.json({
                code:400,
                message: "not working"
            })
        } else if (userData) {
            res.json({
                code:200,
                message: "working",
                data: userData
            })
        } else {
            res.json({
                code:500,
                message: "not getting",
            })
        }
    })
}
