// In this controller Summary detail api was written.
var mongoose = require('mongoose');
var summaryform = mongoose.model('Summary');
var shippingform = mongoose.model('Shipping');
var waterfall = require('async-waterfall');
var cartform = mongoose.model('cart');

module.exports = {
    addSummary: addSummary,
    listSummary: listSummary
}

// In this addsummary detail api was written for that waterfall model was used.
function addSummary(req, res) {
    var user_id = req.swagger.params.id.value
    var finalResponse = {};
    waterfall([
            function (callback) {
                cartform.find({
                    user_id,
                    is_delete: false
                }).sort({
                    date: 'descending'
                }).populate({
                    path: 'product_id',
                    model: 'product',
                }).exec(function (err, data) {

                    if (err) {
                        callback(err);
                    } else if (data) {
                        finalResponse.finalCart = data;
                        // console.log("finalResponse.finalcart", finalResponse.finalCart);
                        callback(null, finalResponse);
                    } else {
                        callback(err);

                    }
                });
            },
            function (finalResponse, callback) { //Get Admin Info
                shippingform.findOne({
                        user_id: user_id,
                    },
                    function (err, data) {
                        if (err) {
                            callback(err);
                        } else {
                            finalResponse.finalShipping = data;

                            record = new summaryform();
                            record.finalCart = finalResponse.finalCart; //left side variables are property names inside schema declaration and right side variables are 'FormControlName' attribute values in index.html
                            record.finalShipping = finalResponse.finalShipping;
                            record.transaction_id = req.body.transaction_id;
                            record.total_amount = req.body.total_amount;
                            record.userid = finalResponse.finalShipping.user_id;



                            record.save();

                            // console.log("finalResponse.finalShipping", finalResponse.finalShipping);
                            callback(res.json({
                                code: 200,
                                data: record
                            }));
                        }
                    });
            },
        ]),
        function (err, record) {
            if (err) {
                res.json({
                    code: 404,
                    message: 'summary not Added',
                    data: err
                })
            } else if (record) {
                res.json({
                    code: 200,
                    data: record
                })
            } else {
                res.json({
                    code: 404,
                    message: 'summary not Added',
                    data: err
                })
            }
        }

}

// This api for listing the summary details.
function listSummary(req, res) {
    summaryform.findOne({
        userid: req.swagger.params.id.value,
        is_delete: false
    }).sort({
        date: 'descending'
    }).exec(function (err, data) {
        if (err) {
            res.json({
                code: 500,
                message: "error",
                data: err
            });
        } else {
            res.json({
                code: 200,
                message: "success",
                data: data
            });
        }
    })
}
